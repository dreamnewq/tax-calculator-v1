package com.taxes.service;


import com.taxes.api.SalaryRequest;
import com.taxes.api.SalaryResponse;
import com.taxes.config.DataSourceConfig;
import com.taxes.db.TaxBand;
import com.taxes.db.TaxSettingsReader;

import java.util.List;

public class CalculatorService {
    private final TaxCalculatorService taxCalculatorService;

    public CalculatorService() {
        TaxSettingsReader taxSettingsReader = new TaxSettingsReader(DataSourceConfig.jdbcTemplate());
        List<TaxBand> taxBands = taxSettingsReader.findByTaxationName("UK_PROGRESSIVE");
        this.taxCalculatorService = new TaxCalculatorService(taxBands);
    }

    public SalaryResponse calc(SalaryRequest request) {
        return taxCalculatorService.calculateTax(request);
    }
}
