package com.taxes.service;


import com.taxes.api.SalaryRequest;
import com.taxes.api.SalaryResponse;
import com.taxes.db.TaxBand;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaxCalculatorService {

    private List<TaxBand> taxBandsSorted;

    private static final int COUNT_OF_MONTHS_IN_YEAR = 12;

    public TaxCalculatorService(List<TaxBand> taxBands) {
        this.taxBandsSorted = new ArrayList<>(taxBands);
        taxBandsSorted.sort(Comparator.comparing(TaxBand::lowerLimit));
    }

    public SalaryResponse calculateTax(SalaryRequest request) {

        // Gross
        var grossAnnualSalary = BigDecimal.valueOf(request.grossAnnualSalary());
        var grossMonthlySalary = grossAnnualSalary.divide(BigDecimal.valueOf(COUNT_OF_MONTHS_IN_YEAR), 2, RoundingMode.HALF_UP);

        // Taxes
        var annualTaxPaid = calculateAnnualTax(grossAnnualSalary, taxBandsSorted);
        var monthlyTaxPaid = annualTaxPaid.divide(BigDecimal.valueOf(COUNT_OF_MONTHS_IN_YEAR), 2, RoundingMode.HALF_UP);

        // Net
        var netAnnualSalary = grossAnnualSalary.subtract(annualTaxPaid);
        var netMonthlySalary = netAnnualSalary.divide(BigDecimal.valueOf(COUNT_OF_MONTHS_IN_YEAR), 2, RoundingMode.HALF_UP);

        grossAnnualSalary =     round(grossAnnualSalary);
        grossMonthlySalary =    round(grossMonthlySalary);
        annualTaxPaid =         round(annualTaxPaid);
        monthlyTaxPaid =        round(monthlyTaxPaid);
        netAnnualSalary =       round(netAnnualSalary);
        netMonthlySalary =      round(netMonthlySalary);

        return new SalaryResponse(grossAnnualSalary, grossMonthlySalary, netAnnualSalary, netMonthlySalary, annualTaxPaid, monthlyTaxPaid);
    }

    private static BigDecimal calculateAnnualTax(BigDecimal grossAnnualSalary, List<TaxBand> taxBandsSorted) {
        BigDecimal tax = BigDecimal.ZERO;

        for (TaxBand band : taxBandsSorted) {
            if (grossAnnualSalary.compareTo(band.lowerLimit()) > 0) {
                BigDecimal taxableAmount = grossAnnualSalary.min(band.upperLimit()).subtract(band.lowerLimit());
                BigDecimal bandTax = taxableAmount.multiply(band.percent()).divide(BigDecimal.valueOf(100));
                tax = tax.add(bandTax);
            }
        }

        return tax;
    }

    private static BigDecimal round(BigDecimal number) {
        return number.setScale(2, BigDecimal.ROUND_HALF_UP);
    }
}
