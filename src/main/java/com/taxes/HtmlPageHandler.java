package com.taxes;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;

import java.util.Map;

public class HtmlPageHandler implements RequestHandler<Object, APIGatewayProxyResponseEvent> {

    @Override
    public APIGatewayProxyResponseEvent handleRequest(Object input, Context context) {
        String htmlContent =
            """
            <!DOCTYPE html>
            <html>
            <head>
                <title>Salary Tax Calculator</title>
                <style>
                    body, html {
                        height: 100%;
                        margin: 0;
                        display: flex;
                        justify-content: center;
                        align-items: center;
                        flex-direction: column;
                    }
                    .container {
                        text-align: center;
                    }
                    input[type="text"] {
                        width: 250px; /* Adjusted for about 60 characters depending on font and font-size */
                    }
                    .results {
                        margin-top: 20px;
                        text-align: left;
                    }
                    .result-item {
                        margin: 5px 0;
                    }
                </style>
            </head>
            <body>
            <div class="container">
                <label for="grossAnnualSalary">Gross Annual Salary:</label>
                <input type="text" id="grossAnnualSalary" placeholder="">
                <button onclick="calculateTax()">Calc</button>
                <div class="results" id="results"></div>
            </div>
            
            <script>
                function calculateTax() {
                    const salaryInput = document.getElementById('grossAnnualSalary').value;
                    const resultsContainer = document.getElementById('results');
            
                    // Map technical parameter names to human-readable names
                    const nameMapping = {
                        "grossAnnualSalary": "Gross Annual Salary",
                        "grossMonthlySalary": "Gross Monthly Salary",
                        "netAnnualSalary": "Net Annual Salary",
                        "netMonthlySalary": "Net Monthly Salary",
                        "annualTax": "Annual Tax",
                        "monthlyTax": "Monthly Tax"
                    };
            
                    fetch('/prod-apigateway/simple-calc-lambda', {
                        method: 'POST',
                        headers: {'Content-Type': 'application/json'},
                        body: JSON.stringify({ "grossAnnualSalary": salaryInput })
                    })
                        .then(response => response.json())
                        .then(data => {
                            resultsContainer.innerHTML = ''; // Clear previous results
                            for (const key in data) {
                                const readableName = nameMapping[key] || key; // Use mapped name if available, otherwise use key as is
                                const value = data[key].toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
                                resultsContainer.innerHTML += `<div class="result-item">${readableName}: ${value}</div>`;
                            }
                        })
                        .catch(error => {
                            resultsContainer.innerHTML = `Error: ${error}`;
                        });
                }
            </script>
            </body>
            </html>
            """;

        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();
        response.setStatusCode(200);
        response.setHeaders(Map.of("Content-Type", "text/html"));
        response.setBody(htmlContent);
        return response;
    }
}
