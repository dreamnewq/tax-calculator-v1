package com.taxes.db;


public record Taxation(Long id, String name, String description) {
}
