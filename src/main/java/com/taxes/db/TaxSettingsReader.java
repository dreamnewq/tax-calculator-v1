package com.taxes.db;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import java.util.List;

public class TaxSettingsReader {

    private final JdbcTemplate jdbcTemplate;

    public TaxSettingsReader(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private final RowMapper<TaxBand> rowMapper = (rs, rowNum) -> {
        Taxation taxation = new Taxation(rs.getLong("taxation_id"), rs.getString("name"), null);
        TaxBand taxBand = new TaxBand(
                rs.getLong("id"),
                rs.getBigDecimal("lower_limit"),
                rs.getBigDecimal("upper_limit"),
                rs.getBigDecimal("percent"),
                taxation
        );
        return taxBand;
    };

    public List<TaxBand> findByTaxationName(String name) {
        String sql = "SELECT tb.*, t.name FROM tax_band tb JOIN taxation t ON tb.taxation_id = t.id WHERE t.name = ?";
        return jdbcTemplate.query(sql, new Object[]{name}, rowMapper);
    }
}
