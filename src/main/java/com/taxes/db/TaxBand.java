package com.taxes.db;




import java.math.BigDecimal;

public record TaxBand(Long id, BigDecimal lowerLimit, BigDecimal upperLimit, BigDecimal percent, Taxation taxation) {
}
