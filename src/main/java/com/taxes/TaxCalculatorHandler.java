package com.taxes;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.google.gson.Gson;
import com.taxes.api.SalaryRequest;
import com.taxes.api.SalaryResponse;
import com.taxes.config.DataSourceConfig;
import com.taxes.service.CalculatorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class TaxCalculatorHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private static final Logger logger = LoggerFactory.getLogger(DataSourceConfig.class);

    private static final Gson gson = new Gson();


    private static final CalculatorService calculatorService;

    static {
        try {
            calculatorService = new CalculatorService();
        } catch (Exception exception) {
            logger.error("Failed to init calculatorService");
            throw exception;
        }
    }

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent request, Context context) {
        SalaryRequest salaryRequest;

        try {
            salaryRequest = gson.fromJson(request.getBody(), SalaryRequest.class);
        } catch (Exception e) {
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(400)
                    .withBody("error : Bad request");
        }

        SalaryResponse response = calculatorService.calc(salaryRequest);

        APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
        responseEvent.setStatusCode(200);
        responseEvent.setBody(gson.toJson(response));
        responseEvent.setHeaders(Map.of("Content-Type", "application/json"));
        return responseEvent;
    }
}
