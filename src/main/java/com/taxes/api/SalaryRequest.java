package com.taxes.api;

public record SalaryRequest(double grossAnnualSalary) {
}
