package com.taxes.api;

import java.math.BigDecimal;

public record SalaryResponse(
        BigDecimal grossAnnualSalary,
        BigDecimal grossMonthlySalary,
        BigDecimal netAnnualSalary,
        BigDecimal netMonthlySalary,
        BigDecimal annualTax,
        BigDecimal monthlyTax
) { }
