package com.taxes.config;

import software.amazon.awssdk.auth.credentials.DefaultCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.secretsmanager.SecretsManagerClient;
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueRequest;
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueResponse;

import com.google.gson.Gson;
import java.util.Map;

public class AwsConfigProperties {

    private static final String secretName = ConfigProperties.getProperty("aws.secretmanager.db-credentias-secretname");
    private static final Region region = Region.of(ConfigProperties.getProperty("aws.region"));

    private static String getSecret() {
        SecretsManagerClient client = SecretsManagerClient.builder()
                .region(region)
                .credentialsProvider(DefaultCredentialsProvider.create())
                .build();

        GetSecretValueRequest valueRequest = GetSecretValueRequest.builder()
                .secretId(secretName)
                .build();
        GetSecretValueResponse valueResponse = client.getSecretValue(valueRequest);
        return valueResponse.secretString();
    }

    public static Map<String, String> getDbCredentials() {
        String secretString = getSecret();
        Gson gson = new Gson();
        return gson.fromJson(secretString, Map.class);
    }
}
