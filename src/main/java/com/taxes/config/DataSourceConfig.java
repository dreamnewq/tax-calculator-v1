package com.taxes.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

public class DataSourceConfig {

    private static final Logger logger = LoggerFactory.getLogger(DataSourceConfig.class);
    public static DataSource dataSource() {
        logger.info("Initializing data source..");
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(AwsConfigProperties.getDbCredentials().get("url"));
        config.setUsername(AwsConfigProperties.getDbCredentials().get("username"));
        config.setPassword(AwsConfigProperties.getDbCredentials().get("password"));

        return new HikariDataSource(config);
    }

    public static JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }
}
